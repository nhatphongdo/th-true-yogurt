﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using TH_Yogurt.Models;
using Mandrill;

namespace TH_Yogurt.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(int? id)
        {
            ViewBag.IsSuccessful = false;
            using (var entities = new THYogurtEntities())
            {
                if (id.HasValue)
                {
                    var user = entities.UserShares.FirstOrDefault(u => u.ID == id);
                    return View(new UserModel()
                    {
                        ShareContent = user.Sharing.Replace("\n", "<br />")
                    });
                }
                else
                {
                    return View();
                }
            }
        }

        [HttpPost]
        public ActionResult Index(UserModel model)
        {
            if (ModelState.IsValid)
            {
                using (var entities = new THYogurtEntities())
                {
                    var user = new UserShare();
                    user.Address = model.Address;
                    user.Fullname = model.Fullname;
                    user.Email = model.Email;
                    user.CreatedOn = DateTime.Now;
                    user.PhoneNumber = model.PhoneNumber;
                    user.Sharing = model.ShareContent;

                    entities.UserShares.Add(user);
                    entities.SaveChanges();

                    ViewBag.IsSuccessful = true;
                    ViewBag.ShareID = user.ID;

                    model.ShareContent = model.ShareContent.Replace("\n", "<br />");
                    return View(model);
                }
            }

            ViewBag.IsSuccessful = false;
            return View(model);
        }

        //public ActionResult SuaChuaAn()
        //{
        //    return View();
        //}

        //public ActionResult SuaChuaUongMenSong()
        //{
        //    return View();
        //}

        //public ActionResult SuaChuaUongTietTrung()
        //{
        //    return View();
        //}

        //public ActionResult CuaHang()
        //{
        //    using (var entities = new THYogurtEntities())
        //    {
        //        return View(entities.Stores.ToList());
        //    }
        //}

        //public ActionResult TruyenThong()
        //{
        //    return View();
        //}

        //public ActionResult SanPhamSuaChuaAn(string id)
        //{
        //    return View("SanPhamSuaChuaAn", "_Layout", id);
        //}

        //public ActionResult SanPhamSuaChuaUongMenSong(string id)
        //{
        //    return View("SanPhamSuaChuaUongMenSong", "_Layout", id);
        //}

        //public ActionResult SanPhamSuaChuaUongTietTrung(string id)
        //{
        //    return View("SanPhamSuaChuaUongTietTrung", "_Layout", id);
        //}

        public ActionResult Invitation()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Invitation(InvitationModel model)
        {
            if (ModelState.IsValid)
            {
                if (model.TrueMart == "-1")
                {
                    model.TrueMart = null;
                    ModelState.AddModelError("TrueMart", "Bạn vui lòng chọn Địa chỉ nhận thư mời");
                    return View(model);
                }

                using (var entities = new THYogurtEntities())
                {
                    var now = new DateTime(2013, 11, 1);
                    //var user = entities.Users.FirstOrDefault(u => u.Email.ToLower() == model.Email.ToLower() && u.CreatedOn >= now);

                    //if (user != null)
                    //{
                    //    ModelState.AddModelError("Email", "Địa chỉ Email này đã được sử dụng");
                    //    return View(model);
                    //}

                    //user = entities.Users.FirstOrDefault(u => u.PhoneNumber.ToLower() == model.PhoneNumber.ToLower() && u.CreatedOn >= now);
                    //if (user != null)
                    //{
                    //    ModelState.AddModelError("PhoneNumber", "Số điện thoại này đã được sử dụng");
                    //    return View(model);
                    //}

                    var user = new User();
                    user.Address = model.Address;
                    user.Fullname = model.Fullname;
                    user.DateOfBirth = model.DateOfBirth;
                    user.Email = model.Email;
                    user.Gender = model.Gender;
                    user.JoinTime = model.JoinTime;
                    //user.JoinTime = "Từ 16 - 18/8";
                    user.CreatedOn = DateTime.Now;
                    user.PhoneNumber = model.PhoneNumber;
                    user.TrueMart = model.TrueMart;

                    entities.Users.Add(user);
                    entities.SaveChanges();

                    var api = new MandrillApi("4awmMsVYtF5LGSy0w8XF1w");
                    var message = new EmailMessage();
                    message.from_email = "post@thtruemart.com.vn";
                    message.from_name = "TH true mart";
                    message.html = string.Format("<html><table width='800' style='width: 800px;'><tr><td style='text-align: center'>Cảm ơn bạn đã đăng ký tham gia chương trình. Xin vui lòng In thư mời hoặc lưu lại trong Điện thoại / Máy tính và mang đến các cửa hàng TH true mart để Tham gia chương trình. Mọi thắc mắc xin vui lòng liên hệ đường dây nóng 1800 54 54 40.</td></tr><tr><td style='align: center;'><a href='{0}' target='_blank'>Click vào đây nếu bạn không xem được hình ảnh gửi kèm</a><br /></td></tr><tr><td><a href='{0}' target='_blank'><img src='{1}' alt='Thu moi' /></a></td></tr></table></html>",
                        Url.Action("ThuMoi", "Home", new
                        {
                            id = user.ID
                        }, "http"), "http://yogurt.thmilk.vn/Letters/" + user.ID.ToString() + ".jpg");
                    message.subject = "Thu moi Tham du chuong trinh dung thu san pham sua chua TH true YOGURT";
                    message.to = new List<EmailAddress>() { new EmailAddress(user.Email) };
                    message.track_clicks = true;
                    api.SendMessage(message);

                    return RedirectToAction("ThuMoi", "Home", new
                    {
                        id = user.ID
                    });
                }
            }

            return View(model);
        }

        public ActionResult ThuMoi(int id)
        {
            using (var entities = new THYogurtEntities())
            {
                var user = entities.Users.FirstOrDefault(u => u.ID == id);

                if (user != null)
                {
                    var imageUrl = "";
                    if (user.TrueMart.ToLower() == "tp. hồ chí minh")
                    {
                        imageUrl = "~/Images/TH_ThuMoi_truemart_TpHCM_11.jpg";
                    }
                    else
                    {
                        imageUrl = "~/Images/TH_ThuMoi_truemart_MienBac_11.jpg";
                    }

                    // Add information
                    //try
                    {
                        var bitmap = new Bitmap(Server.MapPath(imageUrl));

                        // Fill in information
                        var graphics = Graphics.FromImage(bitmap);

                        Font font = new Font("Arial", 12);
                        graphics.DrawString(user.Fullname, font, Brushes.Black, 677, 205);
                        graphics.DrawString((DateTime.Now.Year - user.DateOfBirth.Year).ToString(), font, Brushes.Black, 645, 223);
                        graphics.DrawString(user.Gender, font, Brushes.Black, 681, 240);
                        graphics.DrawString(user.Address, font, Brushes.Black, 669, 258);
                        graphics.DrawString(user.PhoneNumber, font, Brushes.Black, 637, 295);

                        graphics.DrawString(string.Format("THYA00{0:00000}", user.ID), new Font("Arial", 18), Brushes.White, 807, 34);

                        bitmap.Save(Server.MapPath("~/Letters/" + user.ID.ToString() + ".jpg"), System.Drawing.Imaging.ImageFormat.Jpeg);
                        imageUrl = "~/Letters/" + user.ID.ToString() + ".jpg";
                    }
                    //catch (Exception exc)
                    //{

                    //}

                    return View("ThuMoi", "_Layout", Url.Content(imageUrl));
                }
                else
                {
                    return View();
                }
            }
        }
    }
}
