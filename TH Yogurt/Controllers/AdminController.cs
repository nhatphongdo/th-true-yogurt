﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using DevExpress.Web.ASPxPager;
using DevExpress.Web.Mvc;

namespace TH_Yogurt.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FormCollection form)
        {
            if (form["Password"] == "th123#@!")
            {
                Session["Logged"] = true;
                return RedirectToAction("Registration");
            }

            Session.Remove("Logged");

            ViewData["ErrorMessage"] = "Wrong password";
            return View();
        }

        public ActionResult Registration()
        {
            if (Session["Logged"] == null || (bool)Session["Logged"] == false)
            {
                return RedirectToAction("Index");
            }

            return View(GetUsers());
        }

        public ActionResult ExportTo()
        {
            if (Session["Logged"] == null || (bool)Session["Logged"] == false)
            {
                return RedirectToAction("Index");
            }

            return Request.Params["ExportXLS"] != null ? GridViewExtension.ExportToXls(CreateExportGridViewSettings(), GetUsers()) : RedirectToAction("Registration");
        }

        public ActionResult RegistrationPartial()
        {
            return PartialView(GetUsers());
        }

        public static IEnumerable<User> GetUsers()
        {
            var entities = new THYogurtEntities();
            var time = new DateTime(2013, 11, 1);
            return (from user in entities.Users
                    where user.CreatedOn >= time
                    select user).ToList();
        }

        public static GridViewSettings CreateExportGridViewSettings()
        {
            var settings = new GridViewSettings
                               {
                                   Name = "gvExport",
                                   CallbackRouteValues = new { Controller = "Admin", Action = "RegistrationPartial" },
                                   Width = Unit.Percentage(100)
                               };

            var c = settings.Columns.Add("No", "No.");
            c.UnboundType = DevExpress.Data.UnboundColumnType.Integer;

            settings.CustomColumnDisplayText = (sender, e) =>
            {
                if (e.Column.FieldName == "No")
                {
                    e.DisplayText = (e.VisibleRowIndex + 1).ToString();
                }
            };

            settings.Columns.Add("Fullname", "Họ và tên");
            settings.Columns.Add(column =>
                                     {
                                         column.FieldName = "DateOfBirth";
                                         column.Caption = "Ngày sinh";
                                         column.Width = 200;
                                         column.ColumnType = MVCxGridViewColumnType.DateEdit;
                                     });
            settings.Columns.Add("Gender", "Giới tính");
            settings.Columns.Add("PhoneNumber", "Số điện thoại");
            settings.Columns.Add("Address", "Địa chỉ");
            settings.Columns.Add("Email");
            //settings.Columns.Add("Sharing", "Nội dung chia sẻ");
            //settings.Columns.Add("CreatedOn", "Thời gian chia sẻ");
            settings.Columns.Add("TrueMart", "Thành phố đăng ký");
            settings.Columns.Add("JoinTime", "Thời gian đăng ký");

            settings.Caption = "Danh sách thành viên đăng ký";
            settings.Settings.ShowTitlePanel = true;
            settings.SettingsBehavior.AllowSort = true;
            settings.SettingsPager.PageSize = 25;
            settings.SettingsPager.PageSizeItemSettings.Visible = true;
            settings.Settings.ShowFilterRow = true;
            settings.Settings.ShowFooter = true;
            settings.Settings.ShowGroupPanel = true;

            return settings;
        }
    }
}
