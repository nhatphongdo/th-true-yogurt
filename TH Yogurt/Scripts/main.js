﻿$(document).ready(function () {
    // Expand the background
    $('.page .background').css("width", $(window).innerWidth() > 1008 ? $(window).innerWidth() : 1008);
    $('.page .background').css("height", $(window).innerHeight());

    //$('.page header').css("width", $(document).width());

    $(window).resize(function () {
        $('.page .background').css("width", $(window).innerWidth() > 1008 ? $(window).innerWidth() : 1008);
        $('.page .background').css("height", $(window).innerHeight());
        //$('.page header').css("width", $(document).width());
    })
});

function movePrev(step) {
    if (step == 0) {
        return;
    }

    if (step == undefined) {
        step = 1;
    }

    var count = $('.scroll-container .product-detail').length - 1;
    var width = $('.scroll-container .product-detail:eq(0)').width();

    if (parseInt($('.scroll-container .product-detail:eq(0)').css("margin-left")) == -width) {
        $('.scroll-container .product-detail:eq(0)').insertAfter($('.scroll-container .product-detail:eq(' + count + ')'));
        $('.scroll-container .product-detail:eq(' + count + ')').css("margin-left", 0);
    }
    $('.scroll-container .product-detail:eq(0)').animate({
        "margin-left": -width
    },
	function () {
	    $('.scroll-container .product-detail:eq(0)').insertAfter($('.scroll-container .product-detail:eq(' + count + ')'));
	    $('.scroll-container .product-detail:eq(' + count + ')').css("margin-left", 0);

	    var id = $('.scroll-container .product-detail:eq(0)').attr("id").substr(7) - 1;
	    $('.others .item').hide();
	    for (i = 0; i < $('.others .item').length; i++) {
	        if (i != id) {
	            $('.others .item:eq(' + i + ')').fadeIn();
	        }
	    }

	    if (step > 0) {
	        movePrev(step - 1);
	    }
	});
}

function moveNext(step) {
    if (step == 0) {
        return;
    }

    if (step == undefined) {
        step = 1;
    }

    var count = $('.scroll-container .product-detail').length - 1;
    var width = $('.scroll-container .product-detail:eq(0)').width();

    if (parseInt($('.scroll-container .product-detail:eq(0)').css("margin-left")) == 0) {
        $('.scroll-container .product-detail:eq(' + count + ')').insertBefore($('.scroll-container .product-detail:eq(0)'));
        $('.scroll-container .product-detail:eq(0)').css("margin-left", -width);
    }
    $('.scroll-container .product-detail:eq(0)').animate({
        "margin-left": parseInt($('.scroll-container .product-detail:eq(0)').css("margin-left")) + width
    },
	function () {
	    $('.scroll-container .product-detail:eq(' + count + ')').insertBefore($('.scroll-container .product-detail:eq(0)'));
	    $('.scroll-container .product-detail:eq(0)').css("margin-left", parseInt($('.scroll-container .product-detail:eq(1)').css("margin-left")) - width);

	    var id = $('.scroll-container .product-detail:eq(0)').attr("id").substr(7) - 1;
	    $('.others .item').hide();
	    for (i = 0; i < $('.others .item').length; i++) {
	        if (i != id) {
	            $('.others .item:eq(' + i + ')').fadeIn();
	        }
	    }

	    if (step > 0) {
	        moveNext(step - 1);
	    }
	});
}

function moveTo(step) {
    var count = $('.scroll-container .product-detail').length;

    var id = "product" + step;
    var idx = 0;
    for (var i = 0; i < count; i++) {
        if ($('.scroll-container .product-detail:eq(' + i + ')').attr("id") == id) {
            break;
        }
        ++idx;
    }

    if (idx > 0) {
        movePrev(idx);
    }
}

function changeVideo(video, elem) {
    $('.tvc').html("");
    $('.tvc').hide();
    $('.tvc').html("<iframe width='690' height='415' src='" + video + "' frameborder='0' allowfullscreen></iframe>");
    $('.tvc').fadeIn();

    $('.tvcbox .list a').removeClass('selected');
    $(elem).addClass('selected');
}