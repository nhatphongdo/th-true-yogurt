﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TH_Yogurt.Models
{
    public class InvitationModel
    {
        [Required(ErrorMessage = "Bạn vui lòng nhập Họ và tên")]
        public string Fullname
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng nhập Ngày sinh")]
        public DateTime DateOfBirth
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng chọn Giới tính")]
        public string Gender
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng nhập Địa chỉ Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"^([\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+\.)*[\w\!\#$\%\&\'\*\+\-\/\=\?\^\`{\|\}\~]+@((((([a-zA-Z0-9]{1}[a-zA-Z0-9\-]{0,62}[a-zA-Z0-9]{1})|[a-zA-Z])\.)+[a-zA-Z]{2,6})|(\d{1,3}\.){3}\d{1,3}(\:\d{1,5})?)$", ErrorMessage = "Địa chỉ Email không hợp lệ")]
        public string Email
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng nhập Số điện thoại")]
        [StringLength(32, MinimumLength = 8, ErrorMessage = "Số điện thoại không hợp lệ")]
        public string PhoneNumber
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng nhập Địa chỉ")]
        public string Address
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng chọn Địa chỉ nhận thư mời")]
        public string TrueMart
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Bạn vui lòng chọn Thời gian tham gia chương trình")]
        public string JoinTime
        {
            get;
            set;
        }
    }
}